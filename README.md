# How to compile
All scripts from pkgscripts-ng must be executed with root permissions.

## Setup the environment

```
cd pkgscripts-ng
./EnvDeploy -v 6.1 -p avoton
```

## Build & Package
```
pkgscripts-ng/PkgCreate.py -v 6.1 -p avoton -x0 -S -c istatserver
```

# Project Structure

```
istatserver-dsm/
├── build_env/
│   └── ds.${platform}-${version}/
│       └── /usr/syno/
│           ├── bin
│           ├── include
│           └── lib
├── pkgscripts-ng/
└── source/
    ├──istatserver/
    ├── istatserver related source code
    ├── INFO.sh
    ├── PACKAGE_ICON.PNG
    ├── PACKAGE_ICON_256.PNG
    ├── scripts/
    └── SynoBuildConf/
        ├── build
        ├── depends
        └── install
```